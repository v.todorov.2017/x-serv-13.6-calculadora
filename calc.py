# Ejercicio propuesto 15.6 (Calculadora)

def sumar(a , b):   # Función que devuelve la suma de dos valores
    return a + b

def restar(a  , b):  # Función que devuelve la resta de dos valores
    return a - b

print("La suma de 1 y 2 es: " + str(sumar(1,2)))
print("La suma de 3 y 4 es: " + str(sumar(3,4)))
print("La resta de 5 a 6 es: " + str(restar(6,5)))
print("La resta de 7 a 8 es: " + str(restar(8,7)))
